# Database Resources

## Table of Contents
1. [Transactions](#transactions)
2. [Indexes](#indexes)

---

## Transactions
- [Postgres Pro](https://postgrespro.ru/education/books/internals)

## Indexes
- [BookFlow](https://bookflow.ru/indeksy-v-mysql/)