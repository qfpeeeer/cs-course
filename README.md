# Computer Science Course

## Table of Contents

1. [Computer Architecture](#computer-architecture)
2. [OS + Networking](#os--networking)
3. [Databases](#databases)
4. [Security](#security)
5. [Algorithms & Math](#algorithms--math)
6. [Distributed Systems](#distributed-systems)
7. [Additional Resources](#additional-resources)

---

### Computer Architecture
- [CS:APP 3e](http://csapp.cs.cmu.edu/3e/home.html)
- [CMU Schedule](https://www.cs.cmu.edu/afs/cs/academic/class/15213-f16/www/schedule.html)

### OS + Networking
- [OSTEP](https://pages.cs.wisc.edu/~remzi/OSTEP/)
- [Beej's Guide to Network Programming](https://beej.us/guide/bgnet/)
- [Filesystem Hierarchy Standard](http://gaia.cs.umass.edu/kurose_ross/index.php)

### Databases
- [CMU Database Group Playlists](https://www.youtube.com/@CMUDatabaseGroup/playlists)

### Security
- [pwn.college](https://pwn.college)
- [Stanford CS253](https://web.stanford.edu/class/cs253/)

### Algorithms & Math
- [MIT 6.006](https://ocw.mit.edu/courses/6-006-introduction-to-algorithms-spring-2020/video_galleries/lecture-videos/)
- [MIT 6.042J](https://ocw.mit.edu/courses/6-042j-mathematics-for-computer-science-fall-2010/video_galleries/video-lectures/)

### Distributed Systems
- [Designing Data-Intensive Applications](https://www.amazon.com/Designing-Data-Intensive-Applications-Reliable-Maintainable-ebook/dp/B06XPJML5D/?redirectFromSmile=1)
- [YouTube Playlist](https://www.youtube.com/watch?v=cQP8WApzIQQ&list=PLrw6a1wE39_tb2fErI4-WkMbsvGQk9_UB)

### Additional Resources
- [Teach Yourself CS](https://teachyourselfcs.com)

---

- [Books](books/README.md)

- [Golang](golang/README.md)

- [Databases](databases/README.md)

  