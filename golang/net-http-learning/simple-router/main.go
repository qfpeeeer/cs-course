package main

import (
	"fmt"
	"log"
	"net/http"
)

const port = ":7777"

func index(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprintf(w, "Hello world!")
	if err != nil {
		return
	}
}

func main() {
	http.HandleFunc("/", index)
	fmt.Printf("application starting on port %s\n", port)
	err := http.ListenAndServe(port, nil)
	if err != nil {
		log.Printf("failed to start applcation: %s\n", err.Error())
		return
	}
}
