- # Golang Resources

  ## Table of Contents
  1. [Kafka](#kafka)
  2. [Interview Preparation](#interview-preparation)
  3. [Additional Resources](#additional-resources)
  
  ---
  
  ## Kafka
  - [YouTube Kafka Playlist](https://www.youtube.com/watch?v=j4bqyAMMb7o&list=PLa7VYi0yPIH0KbnJQcMv5N9iW8HkZHztH)
  
  ## Interview Preparation
  - [Inner Working or Internals of an Interface in Go](https://golangbyexample.com/inner-working-interface-golang/)
  - [Channel Axioms](https://dave.cheney.net/2014/03/19/channel-axioms)
  - [Scheduling In Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part1.html)
  - [How Map Type Works in Golang](https://www.youtube.com/watch?v=P_SXTUiA-9Y)
  - Where to better use interfaces in golang [link1](https://medium.com/@mbinjamil/using-interfaces-in-go-the-right-way-99384bc69d39), [link2](https://www.digitalocean.com/community/tutorials/how-to-use-interfaces-in-go)
  - [Skills Mentor](https://www.youtube.com/@Skills_mentor/videos)
  
  ## Additional Resources
  - [Go Training](https://github.com/go-training/training)
  - [Go Concurrency Guide](https://github.com/luk4z7/go-concurrency-guide)
  - [Go Patterns](https://github.com/AlexanderGrom/go-patterns)
  - [100 Go Mistakes](https://github.com/teivah/100-go-mistakes)
  - [Go Interview](https://github.com/goavengers/go-interview)

  ## Ideas to pet projects
  - Create custom index for postgresql (smth like B-tree or GIN)