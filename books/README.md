# Books on Computer Science Topics

## Databases

| Title                   | Author     | Language | Link                                                         |
| ----------------------- | ---------- | -------- | ------------------------------------------------------------ |
| Postgresql 15 internals | Egor Rogov | Russian  | [Download](https://drive.google.com/file/d/1pIkjwZrV238YWOlz4uKokp8h0tiSbBIL/view?usp=sharing) |
